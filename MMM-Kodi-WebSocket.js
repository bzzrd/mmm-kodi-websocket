Module.register("MMM-Kodi-WebSocket", {
  ws: null,
  state: null,
  mediaId: null,
  playing: null,


  defaults: {
    host: null,
    port: 9090,
    retryInterval: 5 * 60 * 1000,
    hideWhenPaused: false,
  },

  notificationReceived: function(notification, payload, sender) {
    if (notification == "MODULE_DOM_CREATED") {
      var self = this;
      self.hide();
      if (self.config.host === null) {
        self.sendNotification("SHOW_ALERT", {
          type: "notification",
          title: self.name + " Configuration Error",
          message: "host needs to be set in the configuration file",
        });
        return;
      }
      this.connect();
    }
  },

  getTemplate: function() {
    return "MMM-Kodi-WebSocket.njk";
  },

  getStyles: function() {
    return ["MMM-Kodi-WebSocket.css"];
  },

  getTemplateData: function() {
    var self = this;
    return {
      connected: self.ws !== null,
      state: self.state,
      playing: self.playing,
      formatTime: self.formatTime,
      formatThumbnail: self.formatThumbnail,
      hideWhenPaused: self.config.hideWhenPaused,

    };
  },

  connect: function() {
    var self = this;
    var ws = new WebSocket("ws://@" + self.config.host + ":" + self.config.port);
    ws.onopen = function(evt) {
      console.log("Connected to " + ws.url);
      self.ws = ws;
      self.show();
      self.updateDom();
      ws.send(JSON.stringify({
        "jsonrpc": "2.0",
        "method": "Player.GetProperties",
        "params": {
          "properties": ["speed"],
          "playerid": 0
        },
        "id": "Player.getSpeed"
      }))
    };
    ws.onclose = function(evt) {
      console.log("Disconnected from " + ws.url);
      self.ws = null;
      self.hide();
      self.retryConnect();
    };
    ws.onerror = function(evt) {
      if (self.ws !== null) {
        self.sendNotification("SHOW_ALERT", {
          type: "notification",
          title: self.name + " Error",
          message: "There was an error with the websocket communication",
          timer: 5000
        });
      }
    };
    ws.onmessage = function(message) {
      var parsedMessage = JSON.parse(message.data);
      if (parsedMessage.id == 'AudioGetItem') { // this message is our media update
        self.state = parsedMessage;
        self.mediaId = parsedMessage.result.item.id
      }
      if (parsedMessage.id == 'Player.getSpeed') { // this message is our initial request
        parsedMessage.result.speed !== 0 ? self.playing = true : self.playing = false;
        self.ws.send(JSON.stringify({
          "jsonrpc": "2.0",
          "method": "Player.GetItem",
          "params": {
            "properties": ["title", "album", "artist", "duration", "thumbnail"],
            "playerid": 0
          },
          "id": "AudioGetItem"
        }))
      }
      if (parsedMessage.method == 'Player.OnSpeedChanged' || parsedMessage.method == 'Player.OnPlay') {
        if (self.mediaId !== parsedMessage.params.data.item.id) {
          self.ws.send(JSON.stringify({
            "jsonrpc": "2.0",
            "method": "Player.GetItem",
            "params": {
              "properties": ["title", "album", "artist", "duration", "thumbnail"],
              "playerid": 0
            },
            "id": "AudioGetItem"
          }))
        }
        if (parsedMessage.params.data.player.speed !== 0) {
          self.playing = true;
        } else {
          self.playing = false;
        }
      }

      if (parsedMessage.method == 'Player.OnStop') {
        self.playing = false;
      }
      self.updateDom();
    }
  },

  retryConnect: function() {
    var self = this;
    setTimeout(function() {
      self.connect();
    }, self.config.retryInterval);
  },

  formatTime: function(seconds) {
    var minutes = Math.floor(seconds / 60);
    seconds = seconds - (minutes * 60);
    return minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
  },

  formatThumbnail: function(thumb) {
    var thumbUrl = decodeURIComponent(thumb.substring(8));
    return thumbUrl;
  }
});
