# MMM-KODI-WebSocket

## Huge thanks to buxxi for creating [MMM-MPRIS2-Websocket](https://github.com/buxxi/MMM-MPRIS2-WebSocket), this is a quick and dirty fork to use it with kodi.

[Magic Mirror](https://magicmirror.builders/) Module - A module for Magic Mirror that displays the current playing song on Kodi via WebSocket

![Screenshot](https://gitlab.com/bzzrd/mmm-kodi-websocket/-/raw/master/screenshot.png)


## Install
1. Clone repository into ``../modules/`` inside your MagicMirror folder.
2. Add the module to the Magic Mirror config.
```
{
  module: "MMM-Kodi-WebSocket",
  position: "top_left",
  header: "Playing now",
  config: {
    host : <host of target machine>,
    port : <port of target machine>
  }
},
```
3. Make sure websocket api is enabled on your kodi setup
4. Done!

## Configuration parameters
- ``host`` : The machine that has the info on which song is playing, required
- ``port`` : The port of the remote machine that is running the Kodi default is 9090
- ``retryInterval`` : How long in milliseconds it should wait before trying to make a new connection if the remote server isn't answering or the connection has been dropped, default is 5 minutes
